{
  Script to restore translations
}
unit UserScript;


procedure ProcessNode(node: IInterface; sign: String; field: String);
var
  m: IInterface;
  i: Integer;
begin
  if (Signature(node) <> sign) then
    Exit;

  m := Master(node);

  if (not Assigned(m)) then
    Exit;

  //if (OverrideCount(m) > 1) and (OverrideByIndex(m, 0) <> node) then begin
  //  //AddMessage('Overrides count: ' + IntToStr(OverrideCount(m)));
  //  //m := OverrideByIndex(m, 0);
  //  m := OverrideByIndex(m, OverrideCount(m) - 2);
  //end;

  if ((GetElementEditValues(node, field) = GetElementEditValues(m, field)) or (GetElementEditValues(m, field) = '')) then
    Exit;

  AddMessage('Replace "' + GetElementEditValues(node, field) + '" with "' + GetElementEditValues(m, field) + '"');
  SetElementEditValues(node, field, GetElementEditValues(m, field));
end;


function Process(e: IInterface): integer;
var
  m, t: IInterface;
  i: Integer;
begin
  ProcessNode(e, 'ARMO', 'FULL - Name');
  ProcessNode(e, 'ARMO', 'DESC - Description');

  ProcessNode(e, 'AMMO', 'FULL - Name');

  ProcessNode(e, 'ALCH', 'FULL - Name');

  ProcessNode(e, 'BOOK', 'FULL - Name');
  ProcessNode(e, 'BOOK', 'DESC - Book Text');

  ProcessNode(e, 'CELL', 'FULL - Name');
  
  ProcessNode(e, 'CONT', 'FULL - Name');

  ProcessNode(e, 'DIAL', 'FULL - Name');
  ProcessNode(e, 'INFO', 'NAM1 - Response Text');

  ProcessNode(e, 'FLOR', 'FULL - Name');
  ProcessNode(e, 'FLOR', 'RNAM - Activate Text Override');

  ProcessNode(e, 'INGR', 'FULL - Name');

  ProcessNode(e, 'FURN', 'FULL - Name');

  ProcessNode(e, 'LCTN', 'FULL - Name');

  ProcessNode(e, 'LIGH', 'FULL - Name');

  ProcessNode(e, 'NPC_', 'FULL - Name');
  ProcessNode(e, 'NPC_', 'SHRT - Short Name');

  ProcessNode(e, 'MISC', 'FULL - Name');

  ProcessNode(e, 'MGEF', 'FULL - Name');
  ProcessNode(e, 'MGEF', 'DNAM - Magic Item Description');

  ProcessNode(e, 'SPEL', 'FULL - Name');
  ProcessNode(e, 'SPEL', 'DESC - Description');

  ProcessNode(e, 'RACE', 'FULL - Name');
  ProcessNode(e, 'RACE', 'DESC - Description');

  ProcessNode(e, 'PERK', 'FULL - Name');
  ProcessNode(e, 'PERK', 'DESC - Description');

  ProcessNode(e, 'WEAP', 'FULL - Name');

  ProcessNode(e, 'WRLD', 'FULL - Name');
end;

end.
